package com.example.teste;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		if (android.os.Build.VERSION.SDK_INT > 9){
        	StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        	StrictMode.setThreadPolicy(policy);
        }
		
		ClienteDAO dao = new ClienteDAO();
		
		//ArrayList<Cliente> lista = dao.listarClientes();
		
		//Log.d("Login WS", dao.getLogin("admin", "admin").toString());
		
		Button btnAbreClientes = (Button) findViewById(R.id.btnClientes);
		btnAbreClientes.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				//Log.d("Exemp-lo WS", "Passou onClick");
				Intent it = new Intent(MainActivity.this, ClienteActivity.class);
				//Log.d("Exemplo WS", "Passou onClick 2");
				startActivity(it);
				
				//Log.d("Exemplo WS", "Passou onClick 3");
			}
		});
		
		
		Button btnAbreProdutos = (Button) findViewById(R.id.btnProdutos);
		btnAbreProdutos.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent it = new Intent(MainActivity.this, ProdutoActivity.class);
				startActivity(it);
			}
		});
		
		
//		Button btnTelaLogin = (Button) findViewById(R.id.btnLogin);
//		btnTelaLogin.setOnClickListener(new OnClickListener() {
//			
//			@Override
//			public void onClick(View v) {
//				Intent it = new Intent(MainActivity.this, LoginActivity.class);
//				startActivity(it);
//			}
//		});
		
	}
	
	
	
}
