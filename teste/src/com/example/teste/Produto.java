package com.example.teste;

public class Produto {
	
	private String codigo;
	private String descricao;
	private String foto;
	private String voltagem;
	private String potenciaUso;
	private String potenciaStandby;
	
	public Produto()
	{
		
	}

	public Produto(String codigo, String descricao, String foto,
			String voltagem, String potenciaUso, String potenciaStandby) {
		super();
		this.codigo = codigo;
		this.descricao = descricao;
		this.foto = foto;
		this.voltagem = voltagem;
		this.potenciaUso = potenciaUso;
		this.potenciaStandby = potenciaStandby;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getFoto() {
		return foto;
	}

	public void setFoto(String foto) {
		this.foto = foto;
	}

	public String getVoltagem() {
		return voltagem;
	}

	public void setVoltagem(String voltagem) {
		this.voltagem = voltagem;
	}

	public String getPotenciaUso() {
		return potenciaUso;
	}

	public void setPotenciaUso(String potenciaUso) {
		this.potenciaUso = potenciaUso;
	}

	public String getPotenciaStandby() {
		return potenciaStandby;
	}

	public void setPotenciaStandby(String potenciaStandby) {
		this.potenciaStandby = potenciaStandby;
	}

	@Override
	public String toString() {
		return "Produto [codigo=" + codigo + ", descricao=" + descricao
				+ ", foto=" + foto + ", voltagem=" + voltagem
				+ ", potenciaUso=" + potenciaUso + ", potenciaStandby="
				+ potenciaStandby + "]";
	}

}
