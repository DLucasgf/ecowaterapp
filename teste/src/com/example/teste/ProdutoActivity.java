package com.example.teste;

import java.util.ArrayList;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

public class ProdutoActivity extends Activity {
	
	private ListView lvProduto;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.listaproduto);
		
		Button btnVoltar = (Button) findViewById(R.id.vmainProduto);
		btnVoltar.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		
		//Log.d("Exemplo WS", "btnVoltar");
		ProdutoDAO cDao = new ProdutoDAO();
		lvProduto = (ListView) findViewById(R.id.lvProduto);
		ArrayList<Produto> lista = cDao.listarProdutos();
		//Log.d("Exemplo WS", "array");
		ArrayAdapter<Produto> adpProduto = new ArrayAdapter<Produto>(this, android.R.layout.simple_list_item_1, lista);
		//Log.d("Exemplo WS", "adp");
		lvProduto.setAdapter(adpProduto);
		//Log.d("Exemplo WS", "set");
	}
	
	/*
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.produto, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	*/
}
