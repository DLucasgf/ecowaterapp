package com.example.teste;

import java.util.ArrayList;
import java.util.Vector;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import android.util.Log;

public class ClienteDAO {
	
	//private static final String URL = "http://192.168.1.5/Lucas/ws/soap.php?wsdl";
	//private static final String URL = "http://150.164.152.247/Lucas/ws/soap.php?wsdl";
	private static final String URL = "http://10.0.2.2/soap.php?wsdl";
	private static final String NAMESPACE = "urn:server.ecowater";
	
	private static final String LISTAR_CLIENTES = "listaClientes";
	//private static final String GET_LOGIN = "getLogin";
	
		
	public ArrayList<Cliente> listarClientes(){
		ArrayList<Cliente> lista = new ArrayList<Cliente>();
		
		SoapObject buscarClientes = new SoapObject(NAMESPACE, LISTAR_CLIENTES);
		
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		
		envelope.setOutputSoapObject(buscarClientes);
		
		envelope.implicitTypes = true;
		
		HttpTransportSE http = new HttpTransportSE(URL);
		
		try {
			http.call("urn:" + LISTAR_CLIENTES, envelope);
			
			//SoapObject teste = (SoapObject) envelope.getResponse();
			
			//Log.d("WS Direto AS String", teste.getPropertyAsString(0));
			//Log.d("WS Direto count", teste.getPropertyCount() + "");
			
			
			Vector<SoapObject> resposta = (Vector<SoapObject>) envelope.getResponse();
			
			Cliente cliente;
			
			cliente = new Cliente();
			
			for (SoapObject soapObject : resposta) {
				cliente = new Cliente();
				
				//SoapObject item = (SoapObject) soapObject.getPrimitiveProperty("item");
				
				//Log.d("erro", "teste nome: " + soapObject.getPrimitiveProperty("nome").toString());
				
//				cliente.setNome(soapObject.getPrimitiveProperty("nome").toString());
//				cliente.setCpf(soapObject.getPrimitiveProperty("cpf").toString());
//				cliente.setEmail(soapObject.getPrimitiveProperty("email").toString());
//				cliente.setSenha(soapObject.getPrimitiveProperty("senha").toString());
//				cliente.setEndereco(soapObject.getPrimitiveProperty("endereco").toString());
//				cliente.setNascimento(soapObject.getPrimitiveProperty("nascimento").toString());
				
				//hotel.setCodHotel(Integer.parseInt(soapObject.getProperty("codHotel").toString()));
				//hotel.setValor_Diaria(Float.parseFloat(soapObject.getProperty("valor_Diaria").toString()));
				cliente.setNome(soapObject.getPrimitiveProperty("nome").toString());
				cliente.setCpf(soapObject.getPrimitiveProperty("cpf").toString());
				cliente.setEmail(soapObject.getPrimitiveProperty("email").toString());
				cliente.setSenha(soapObject.getPrimitiveProperty("senha").toString());
				cliente.setEndereco(soapObject.getPrimitiveProperty("endereco").toString());
				cliente.setNascimento(soapObject.getPrimitiveProperty("nascimento").toString());
				
				lista.add(cliente);
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Log.d("erro DAO", "cliente DAO exception" + e.getMessage() + e.toString());
			return null;
		} 	
		
		return lista;
		
	}
	
	public ArrayList<Cliente> listarClientesLocal(){
		ArrayList<Cliente> lista = new ArrayList<Cliente>();
		
		Cliente cliente;
		
		for (int i = 0; i < 5; i++) {
			cliente = new Cliente();
			
			cliente.setNome("nome " + i);
			cliente.setCpf("cpf " + i);
			cliente.setEmail("email " + i);
			cliente.setSenha("senha " + i);
			cliente.setEndereco("endereco " + i);
			cliente.setNascimento("nascimento " + i);
			
			lista.add(cliente);
		}	
		
		return lista;
		
	}
	
	
//	public Cliente getLogin(String email, String senha)
//	{
//		
//		SoapObject getLogin = new SoapObject(NAMESPACE, GET_LOGIN);
//		
//		SoapObject cli = new SoapObject(NAMESPACE, "usuario");
//		cli.addProperty("email", email);
//		cli.addProperty("senha", senha);
//		
//		getLogin.addSoapObject(cli);
//		
//		
//		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
//		
//		envelope.setOutputSoapObject(getLogin);
//		
//		envelope.implicitTypes = true;
//		
//		HttpTransportSE http = new HttpTransportSE(URL);
//		
//		try {
//			http.call("urn:" + GET_LOGIN, envelope);
//			//Log.d("WS Direto AS String", teste.getPropertyAsString(0));
//			//Log.d("WS Direto count", teste.getPropertyCount() + "");
//			
//			SoapPrimitive resposta = (SoapPrimitive) envelope.getResponse();
//			
//			Cliente tmp = new Cliente();
//			tmp.setNome("OK");
//			
//			return tmp;
//			
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//			Log.d("erro DAO", "cliente DAO exception" + e.getMessage() + e.toString());
//			return null;
//		} 	
//		
//	}
	
}
