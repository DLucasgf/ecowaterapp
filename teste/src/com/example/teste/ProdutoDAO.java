package com.example.teste;

import java.util.ArrayList;
import java.util.Vector;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import android.util.Log;

public class ProdutoDAO {
	
	private static final String URL = "http://10.0.2.2/soap.php?wsdl";
	private static final String NAMESPACE = "urn:server.ecowater";
	
	private static final String LISTAR_PRODUTOS = "listaProdutos";
		
	public ArrayList<Produto> listarProdutos(){
		ArrayList<Produto> lista = new ArrayList<Produto>();
		
		SoapObject buscarProdutos = new SoapObject(NAMESPACE, LISTAR_PRODUTOS);
		
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		
		envelope.setOutputSoapObject(buscarProdutos);
		
		envelope.implicitTypes = true;
		
		HttpTransportSE http = new HttpTransportSE(URL);
		
		try {
			http.call("urn:" + LISTAR_PRODUTOS, envelope);
			
			//Log.d("WS Direto AS String", teste.getPropertyAsString(0));
			//Log.d("WS Direto count", teste.getPropertyCount() + "");
			
			
			Vector<SoapObject> resposta = (Vector<SoapObject>) envelope.getResponse();
			
			Produto produto;
			
			for (SoapObject soapObject : resposta) {
				produto = new Produto();
				
				//Log.d("erro", "teste nome: " + soapObject.getPrimitiveProperty("nome").toString());
				
				produto.setCodigo(soapObject.getPrimitiveProperty("codigo").toString());
				produto.setDescricao(soapObject.getPrimitiveProperty("descricao").toString());
				produto.setFoto(soapObject.getPrimitiveProperty("foto").toString());
				produto.setVoltagem(soapObject.getPrimitiveProperty("voltagem").toString());
				produto.setPotenciaUso(soapObject.getPrimitiveProperty("potenciaUso").toString());
				produto.setPotenciaStandby(soapObject.getPrimitiveProperty("potenciaStandby").toString());
				
				lista.add(produto);
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Log.d("erro DAO", "produto DAO exception" + e.getMessage() + e.toString());
			return null;
		} 	
		
		return lista;
		
	}
	
	public ArrayList<Produto> listarProdutosLocal(){
		ArrayList<Produto> lista = new ArrayList<Produto>();
		
		Produto produto;
		
		for (int i = 0; i < 5; i++) {
			produto = new Produto();
			
			produto.setCodigo("codigo " + i);
			produto.setDescricao("descricao " + i);
			produto.setFoto("foto " + i);
			produto.setVoltagem("voltagem " + i);
			produto.setPotenciaUso("potenciaUso " + i);
			produto.setPotenciaStandby("potenciaStandby " + i);
			
			lista.add(produto);
		}	
		
		return lista;
		
	}
	
}
